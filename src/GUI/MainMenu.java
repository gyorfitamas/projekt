
package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class MainMenu extends JFrame{
    private final JPanel MAINSCREEN_PANEL;
    private Const.Settings SETTINGS;
    private BufferedImage backgroundImage;
    private MainMenuListener listener;
    private JButton newGame, options, exit;
    private JLabel padding;
    
    public MainMenu() {
        SETTINGS = Const.Settings.getInstance();
        listener = new MainMenuListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setUndecorated(true);
        setVisible(true);
        setAlwaysOnTop(true);
        padding= new JLabel();
        padding.setPreferredSize(new Dimension(getWidth()/8, 20));
        padding.setMinimumSize(new Dimension(getWidth()/8, 20));
        add(padding, BorderLayout.WEST);
        setSize( SETTINGS.SCREEN_WIDTH,SETTINGS.SCREEN_HEIGHT);
        try {
            backgroundImage = ImageIO.read(new File("Ant.jpg"));
        } catch (IOException ex) {
            System.out.println("Background image not found!");
        }
        
        MAINSCREEN_PANEL = new JPanel(){
            @Override
            public void paint(Graphics gr){
                super.paint(gr);
                gr.drawImage(backgroundImage, 0, 0, this);
                
            }
        };
        MAINSCREEN_PANEL.setLayout(new BoxLayout(MAINSCREEN_PANEL, BoxLayout.Y_AXIS));
        add(MAINSCREEN_PANEL, BorderLayout.CENTER);
        
        exit= new JButton("Exit");
        exit.addMouseListener(listener);
        
        newGame = new JButton("Start new game");
        newGame.addMouseListener(listener);
     //   newGame.setPreferredSize(new Dimension(500,200));
        options = new JButton("Options");
        options.addMouseListener(listener);
      //  
        
        
        MAINSCREEN_PANEL.add(newGame);
        MAINSCREEN_PANEL.add(options);
        MAINSCREEN_PANEL.add(exit);
        newGame.setMaximumSize(new Dimension(getWidth(),30));
        options.setMaximumSize(new Dimension(getWidth(),30));
        exit.setMaximumSize(new Dimension(getWidth(),30));
        
        validate();
                
    }
    
     public BufferedImage getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(BufferedImage backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public MainMenuListener getListener() {
        return listener;
    }

    public void setListener(MainMenuListener listener) {
        this.listener = listener;
    }

    public JButton getNewGame() {
        return newGame;
    }

    public void setNewGame(JButton newGame) {
        this.newGame = newGame;
    }

    public JButton getOptions() {
        return options;
    }

    public void setOptions(JButton options) {
        this.options = options;
    }

    public JButton getExit() {
        return exit;
    }

    public void setExit(JButton exit) {
        this.exit = exit;
    }

    void loadOptions() {
        remove(MAINSCREEN_PANEL);
        add(new GameOptionsPanel(this), BorderLayout.CENTER);
        repaint();
        validate();
    }
    
}
