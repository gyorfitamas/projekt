package GUI;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class GameOptionsPanel extends JPanel{
    
    MainMenu mainMenu;
    public GameOptionsPanel(MainMenu mm){
        mainMenu=mm;
        setLayout(new BorderLayout());
        add(new JButton("Exit"), BorderLayout.SOUTH);
        validate();
    }
}
