
package GUI;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainMenuListener implements MouseListener{
    private MainMenu mainMenu;
    public MainMenuListener(MainMenu mm){
        mainMenu = mm;
    }
    @Override
    public void mouseClicked(MouseEvent me) {
        if (me.getSource()==mainMenu.getExit()){
            System.exit(0);
        }
        
        if (me.getSource()==mainMenu.getOptions()){
            mainMenu.loadOptions();
        }
        
        if ( me.getSource()==mainMenu.getNewGame()){
            
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }
    
}
