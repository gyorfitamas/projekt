package Const;

import java.awt.Dimension;
import java.awt.Toolkit;

public class Settings {
    private static final Settings INSTANCE = new Settings();
    
    public final Dimension SCREEN_SIZE;
    public final int SCREEN_WIDTH;
    public final int SCREEN_HEIGHT;
    
    private Settings(){
        SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
        SCREEN_WIDTH = (int)SCREEN_SIZE.getWidth();
        SCREEN_HEIGHT = (int)SCREEN_SIZE.getHeight();
    }
    
    public static Settings getInstance(){
        return INSTANCE;
    }
}
